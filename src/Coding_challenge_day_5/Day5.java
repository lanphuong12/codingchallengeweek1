package Coding_challenge_day_5;

import java.util.ArrayList;
import java.util.Scanner;

public class Day5 {

//	As a user I should be able to register and login to the application.
//	As a user I can be either the client or visitor.
//	AS a visitor i can only see the tasks assigned to me
//	As a client I should be able to add the task into an array
//	As a client I should be able to update the task in the array
//	As a client I should be able to delete the task in the array
//	As a client I should be able to search a task from an array
//	As a client I should be able to assign the task to a visitor. 
//	As a client I should be able to assign the completion date to the task
//	As a user(client or visitor) I should be able to arrange my task in any order(increasing or descending) of the date.
//	As a visitor I should be able to mark the task as completed .
//	As a visitor I should be able to see the completed or incomplet tasks.

	public static ArrayList<User> arrUser = new ArrayList<>();
	public static void Login(ArrayList<Task> arrTask) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < arrUser.size(); i++) {
				if (userString.equals(arrUser.get(i).getUsername())
						&& passString.equals(arrUser.get(i).getPassword())) {
					if (arrUser.get(i).getPosition().equals("Client")) {
						Client c = new Client();
						c.MenuClient(arrTask);
					} else if (arrUser.get(i).getPosition().equals("Visitor")) {
						Visitor v = new Visitor();
						v.MenuVisitor(arrUser.get(i).getUsername(), arrTask);
					}
				} else {
					tt = false;
				}
			}
			if (tt = false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt = false);
	}
	
	public static void main(String[] arg) {
		ArrayList<Task> arrTask = new ArrayList<>();
		User user = new User("user1", "password1", "Client");
		arrUser.add(user);
		User user1 = new User("user2", "password2", "Visitor");
		arrUser.add(user1);
		User user2 = new User("user3", "password3", "Client");
		arrUser.add(user2);
		
		Login(arrTask);

	}
}
