package CodingChallengeWeek1;

import java.util.ArrayList;
import java.util.Scanner;

public class CodingChallengeWeek1 {

	public static void Login(ArrayList<UserAtributes> user, ArrayList<BookAttributes> arrBook) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName())
						&& passString.equals(user.get(i).getPassword())) {
					UserImpl userImpl = new UserImpl();
					userImpl.Menu(arrBook, user, userString);
				} else {
					tt = false;
				}
			}
			if (tt == false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt == false);
	}

	public static void main(String[] args) {
		ArrayList<BookAttributes> arrBookAttributes = new ArrayList<>();
		arrBookAttributes.add(new BookAttributes(1, "Đắc nhân tâm", "Dale Carnegie", "Quyển sách đưa ra các lời khuyên về cách thức cư xử, ứng xử và giao tiếp với mọi người để đạt được thành công trong cuộc sống"));
		arrBookAttributes.add(new BookAttributes(2, "Quẳng gánh lo đi và vui sống", "Dale Carnegie", "Cuốn sách bao gồm các nội dung xoay quanh các chủ đề chính sau: Phương cách để trị ưu phiền, Cách phân tích những vấn đề rắc rối, Diệt tất ưu phiền đi,đừng để nó diệt ta"));
		arrBookAttributes.add(new BookAttributes(3, "Cha giàu, Cha nghèo", "Robert Kiyosaki", "Cuốn sách là một câu chuyện, chủ yếu nói về sự giáo dục mà Kiyosaki đã nhận được tại Hawaii"));

		ArrayList<UserAtributes> arruserAtributes = new ArrayList<>();
		ArrayList<Integer> arrFavoriteBookArrayList1 = new ArrayList<>();
		
		arrFavoriteBookArrayList1.add(1);
		arrFavoriteBookArrayList1.add(2);
		arruserAtributes.add(new UserAtributes(1, "user1", "123", "dao.phuong@hcl.com", arrFavoriteBookArrayList1));
		
		ArrayList<Integer> arrFavoriteBookArrayList2 = new ArrayList<>();
		arrFavoriteBookArrayList2.add(1);
		arrFavoriteBookArrayList2.add(3);
		arruserAtributes.add(new UserAtributes(1, "user2", "123", "dao.phuong@hcl.com", arrFavoriteBookArrayList2));
		
		ArrayList<Integer> arrFavoriteBookArrayList3 = new ArrayList<>();
		arrFavoriteBookArrayList3.add(2);
		arrFavoriteBookArrayList3.add(3);
		arruserAtributes.add(new UserAtributes(1, "user3", "123", "dao.phuong@hcl.com", arrFavoriteBookArrayList2));
		
		Login(arruserAtributes, arrBookAttributes);

		
	}

}
