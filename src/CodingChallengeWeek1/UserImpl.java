package CodingChallengeWeek1;

import java.util.ArrayList;
import java.util.Scanner;

public class UserImpl {

	MagicOfBooks magicOfBooks = new MagicOfBooks();
	Scanner scanner = new Scanner(System.in);

	CodingChallengeWeek1 week1 = new CodingChallengeWeek1();

	public void Menu(ArrayList<BookAttributes> arrBook, ArrayList<UserAtributes> arrUser, String Username) {
		System.out.println("Menu");
		System.out.println("1. Display list book");
		System.out.println("2. Display my favorite book ");
		System.out.println("3. Search book by id ");
		System.out.println("4. Sign out ");
		System.out.println("0. Exit ");
		System.out.print("Enter your choice: ");
		int ch = Integer.parseInt(scanner.nextLine());
		System.out.println();
		switch (ch) {
		case 0:
			System.out.println("Stop programing!!!");
			System.exit(0);
			break;
		case 1:
			DisplayListBook(arrBook);
			Menu(arrBook, arrUser, Username);
			break;
		case 2:
			DisplayFavoriteBook(arrBook, arrUser, Username);
			Menu(arrBook, arrUser, Username);
			break;
		case 3:
			SearchBookbyID(arrBook, Username, arrUser);
			Menu(arrBook, arrUser, Username);
			break;
		case 4:
			week1.Login(arrUser, arrBook);
			break;
		default:
			UserImpl tuan1 = new UserImpl();
			System.out.println("Incorrect input format!");
			week1.Login(arrUser, arrBook);
			break;
		}
	}

	public void DisplayListBook(ArrayList<BookAttributes> arrBook) {

		for (BookAttributes book : arrBook) {
			System.out.println(book.toString());
		}
	}

	public void DisplayFavoriteBook(ArrayList<BookAttributes> arrBook, ArrayList<UserAtributes> arrUser,
			String username) {
		for (UserAtributes user : arrUser) {
			if (user.getUserName().trim().equals(username)) {
				for (int i : user.getFavourite()) {
					for (int j = 0; j < arrBook.size(); j++) {
						if (arrBook.get(j).getBookId() == i) {
							System.out.println(arrBook.get(j).toString());
						}
					}
				}
			}
		}
	}

	public void SearchBookbyID(ArrayList<BookAttributes> arrBook, String username, ArrayList<UserAtributes> arrUser) {
		String tmpStr = "";
		do {
			System.out.print("Enter ID_book you want to search: ");
			int id = scanner.nextInt();
			System.out.println();
			magicOfBooks.SearchbyID(arrBook, id);

			System.out.print("Do you want to see detail the book? Enter 'Y' to see detail -> ");
			String tmp = scanner.next();
			if (tmp.trim().equals("Y")) {
				magicOfBooks.DisplayBookbyID(arrBook, id);
			}

			System.out.print("Do you like it? Enter 'Y' to add book in list favorite book ");
			String tmpString = scanner.next();
			if (tmpString.trim().equals("Y")) {
				for (int a = 0; a < arrUser.size(); a++) {
					if (arrUser.get(a).getUserName().trim().equals(username)) {
						Boolean checkBoolean = true;
						ArrayList<Integer> listtmp = arrUser.get(a).getFavourite();
						System.out.println(listtmp.size());
						for (int i : listtmp) {
							if (id == i) {
								checkBoolean = false;
							}
						}
						if (checkBoolean) {
							arrUser.get(a).getFavourite().add(id);
						} else {
							System.out.println("You liked this book before!");
						}
					}
				}
			}
			System.out.println("Do you want to continuous? Enter 'Y' to continous -> ");
			tmpStr = scanner.next();
		} while (tmpStr.trim().equals("Y"));
	}
	
}
